#include<ctime>
#include<cstdlib>
#include<vector>
#include<list>
#include<algorithm>
#include<iomanip>
#include<iostream>

using namespace std;

int main(){
    int max, min, numCustomer, maxSer, minSer, wait, cashier, arrive;
    float avrgTime=0, avg;
    vector<int> arriveTime;
    vector<int>serviceTime;
    vector<int>waitTime;
    vector<int>leftTime;
    list<int> queue;
    
    cout << "Max customer : " ;         //to input maximum number of customer
    cin >> max;
    cout << "Min customer : " ;         //to input minimum number of customer
    cin >> min;
    cout << "Max service time : " ;     //to input maximum service time per customer
    cin >> maxSer;
    cout << "Min service Time : " ;     //to input minimum service time per customer
    cin >> minSer;
    cout << "number of cashier : ";     //to input the number of cashier that is available
    cin >> cashier;
    
    srand(time(0));
    
    numCustomer = (rand()%(max-min))+1; //to random number of customers
    if(numCustomer<min)
    {
        numCustomer =min;               //if the random number of customers is less than the minimum make it to be the minimum
    }
    cout << "number of customer : " << numCustomer << endl;
    
    //to random customer' arrived time
    for(int i=0; i<numCustomer; i++){
        arrive = rand()%241;
        if(arrive<0)
        {
            arrive= 0;
        }
        arriveTime.push_back(arrive);
        sort(arriveTime.begin(), arriveTime.end()+1);
    }
    //to set service time of each customer
    for(int i=0; i<numCustomer; i++){
        serviceTime.push_back(rand()%maxSer);
        if(serviceTime.at(i)<minSer)
        {
            serviceTime.at(i)=minSer;
        }
    }
    
    //waittime and lefttime
    for(int i=0; i<numCustomer; i++){
        if(i < cashier){
            waitTime.push_back(0);
            leftTime.push_back(arriveTime.at(i)+serviceTime.at(i)); //left time is arriving time plus time used for service
            avrgTime = avrgTime + waitTime.at(i);   //time used is the sum of waiting time and previos customer average time
            queue.push_back(leftTime.at(i));
            queue.sort();
        }else{
            wait = queue.front()-arriveTime.at(i);
            if(wait < 0)
            {
                waitTime.push_back(0);
                leftTime.push_back(arriveTime[i]+serviceTime[i]);
                queue.push_back(leftTime.at(i)); //store left time
                queue.pop_front(); //remove the least lefttime
                queue.sort(); //sort left time to see which cashier is availble
                avrgTime = avrgTime + waitTime.at(i);
                
            }
            else
            {
                waitTime.push_back(queue.front() - arriveTime.at(i));
                leftTime.push_back(arriveTime.at(i) + serviceTime.at(i) + waitTime.at(i));
                queue.push_back(leftTime.at(i));
                queue.pop_front();
                queue.sort();
                avrgTime = avrgTime + waitTime.at(i);
                
            }
        }
        
        cout << "\nCustomer : " << i+1 << setw(4) << " - arrive time : " << arriveTime.at(i) << setw(17) << " - wait time : " << waitTime.at(i) << setw(17) << " - left time : " << leftTime.at(i);
    }
    avg = avrgTime/numCustomer;
    cout << "\n\naverage wait time : " << avg << endl;
    
    return 0;
}
